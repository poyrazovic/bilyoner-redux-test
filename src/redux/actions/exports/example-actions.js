import {
	TEST, TEST2,
} from '../types';

export const testAction = () => dispatch => {
	dispatch({
		type: TEST,
		payload: false,
	});
};

export const testAction2 = () => dispatch => {
	dispatch({
		type: TEST2,
		payload: true,
	});
};