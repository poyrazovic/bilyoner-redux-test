import {
	TEST,
	TEST2
} from '../../actions/types';

const INITIAL_STATE = {
	data: [{
		'id': 1,
		'title': 'Test'
	}]
};

const exampleReducers = (state = INITIAL_STATE, action) => {
	switch (action.type) {
		case TEST:
			return {...state, data: action.payload};
		case TEST2:
			return {...state, data: action.payload};
		default:
			return state;
	}
};

export default exampleReducers;
