import { combineReducers } from 'redux';
import exampleReducers from "./exports/example-reducers";

const allReducers = combineReducers({
	exampleReducers,
});

export default allReducers;
