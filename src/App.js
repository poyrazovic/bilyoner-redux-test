import React, { Component } from 'react';
import './App.css';
import Accordion from "./components/Accordion/Accordion";

class App extends Component {
  render() {
    return (
      <div className={'container'}>
        <Accordion />
      </div>
    );
  }
}

export default App;
