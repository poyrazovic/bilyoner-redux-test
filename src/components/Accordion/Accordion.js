import React, { Component } from 'react';
import { connect } from 'react-redux';
import {testAction, testAction2} from "../../redux/actions";

class Accordion extends Component {
	constructor(props) {
		super(props);
		this.state = {
		
		};
	}
	
	list() {
		return this.props.data.map((item, index) => {
			return <li key={index}>{item.id + ' - ' + item.title}</li>;
		});
	}
	
	render() {
		return (
			<ul>
				{ this.list() }
			</ul>
		);
	}
}

const mapStateToProps = ({ exampleReducers }) => {
	const {
		data
	} = exampleReducers;
	return {
		data
	};
};

export default connect(mapStateToProps, {
	testAction,
	testAction2,
})(
	Accordion
);